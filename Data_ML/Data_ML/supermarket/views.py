from django.shortcuts import render
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http import HttpResponse, JsonResponse
from .checkout import Checkout

RULES = {
  'A': {'price': 50, 'discount_threshold': 3, 'discount_price': 130},
  'B': {'price': 30, 'discount_threshold': 2, 'discount_price': 45},
  'C': {'price': 20},
  'D': {'price': 15}
}
products = []
item_cart = []
total_price = 0
# Create your views here.
@method_decorator(csrf_exempt, name='dispatch')
class CheckoutView(View):
    def get(self, request, *args, **kwargs):
        products.clear()
        item_cart.clear()
        print(item_cart)
        return render(request, 'supermarket/checkout.html')
   
    def post(self, request, *args, **kwargs):
        total_price = 0
        context = {}
        self.co = Checkout(RULES)
        if 'item_code' in request.POST:
            item = request.POST.get('item_code')
            products.append(item)
            item_data = RULES.get(item)
            item_cart.append(item_data)
            for i in item_cart:
                total_price += i['price']
            context['total_price'] = total_price
            context['item_cart'] = item_data
            return JsonResponse(context)
        if 'calc' in request.POST:
            for i in products:
                self.co.add_or_scan(i)
            total = self.co.total()
            print(total)
            context['discounted_price'] = total
        return JsonResponse(context)

