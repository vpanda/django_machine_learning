from django.urls import path
from django.conf.urls import url

from . import views
from .views import CheckoutView
app_name = 'supermarket'

urlpatterns = [
    
    url(r'^checkout/$', CheckoutView.as_view(),name='checkout'),



]