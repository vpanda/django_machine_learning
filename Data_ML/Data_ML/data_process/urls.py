from django.urls import path
from django.conf.urls import url

from . import views
app_name = 'data_process'

urlpatterns = [
    url(r'^homepage/$', views.homepage,name='homepage'),
    url(r'^inputs/$', views.csv_file_receive,name='inputs'),
    url(r'^process_data/$', views.process_data,name='process_data'),
    url(r'^$', views.home,name='home'),



]