from django.db import models

# Create your models here.
class ProjectList(models.Model):
    project_name = models.CharField(max_length=100, blank=True, null=True)