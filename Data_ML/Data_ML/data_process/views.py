from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, JsonResponse
import pandas as pd
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import SelectKBest, chi2, f_classif, f_regression, SelectFromModel
import numpy as np
from sklearn.preprocessing import Imputer
from django.views.decorators.csrf import csrf_exempt
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer
import json
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import ExtraTreesClassifier
# Create your views here.


def home(request):
    return render(request, 'data_process/home.html')

def homepage(request):
    return render(request, 'data_process/page1.html')
@csrf_exempt
def csv_file_receive(request):
    context = {}
    # csv_file = request.FILES['data_file']
    # df = pd.read_csv(csv_file)
    dframe = uploadedfile(request)
    dframe = dframe.dropna()
    if dframe is not None:
        data = dframe.to_json(orient='records')
        context['header'] = dframe.columns.tolist()
        context['df2']= data
        return render(request, 'data_process/page2.html', context)
    if request.POST.getlist('sel[]'):
        X = request.POST.getlist('sel[]')
        print(X)
        return render(request, 'data_process/page2.html', context)
    return render(request, 'data_process/page2.html', context)
    



def process_data(request):
    context = {}
    dframe = uploadedfile(request)
    dframe = dframe.dropna()
    X = request.POST.getlist('ivsel')
    y = request.POST.get('dvsel')
    X = dframe[X]
    y = dframe[y]
    print('X', X)
    print('y', y)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    regressor = LinearRegression()
    regressor.fit(X_train,y_train)
    y_pred = regressor.predict(X_test)
    # print(y_pred)
    # data = X_test.to_json(orient='records')
    # context['header'] = X_test.columns.tolist()
    # context['df2']= data

    bestfeatures = SelectKBest(score_func=chi2, k=5)
    fit = bestfeatures.fit(X,y)
    dfscores = pd.DataFrame(fit.scores_)
    dfcolumns = pd.DataFrame(X.columns)
    featureScores = pd.concat([dfcolumns,dfscores],axis=1)
    featureScores.columns = ['Specs','Score']
    # print(featureScores.nlargest(5,'Score'))

    model = ExtraTreesClassifier()
    model.fit(X,y)
    print("Importancess>>>>>", model.feature_importances_)
    feat_importances = pd.Series(model.feature_importances_, index=X.columns)
    importance = feat_importances.nlargest(20)
    context['importance']=importance.to_json(orient='index')
    
    # y_cord = json.dump(y_pred)
    # print(x_cord)
    # print(y_cord)



    return render(request, 'data_process/page2.html', context)
    # return render(request, 'data_process/page2.html', context)



def uploadedfile(req):
    print("Enetered func")
    if 'data_file' in req.FILES:
        csv_file = req.FILES['data_file']
        print("csv fileeeeeeeee")
        df = pd.read_csv(csv_file)
        return df
    print("None Data")
    return None