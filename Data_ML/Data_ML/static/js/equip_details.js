date1 = []
ret_temp = []
ins_temp = []
supp = []


function getdetails(which_equip, url) {
    var equip = which_equip;
    var type = '';
    $.ajax({
       type: "GET",
       url: "/api/gt/"+which_equip+"/",
       success: function (data) {
          $( "#oven" ).empty();
          if(data.type != "FAN"){
             $("#controls").hide();
          }
          else{
             $("#controls").show();
          }
          $("#redirect_link").attr('href', url);
          $("#redirect_link").text(url);
          $("#equip_img").attr('src',data.image);
          $("#rightpanelequipid").html(data.name);
          $("#rightpanel").modal();
          $('#equip_name').html(data.des);
          $('#panel_type').html(data.type);
          $('#panel_model').html(data.name);
          $('#panel_spec').html(data.facility.facility_name);
          $('#panel_manf').html(data.spec);
          $("#qr_code").attr('src', data.qr_image);
          $("#equip_analysis").attr('href', '/GT_Divison/'+which_equip+'/analysis/');
          $("#equip_main").attr('href','/GT_Divison/'+which_equip+'/maintenance/');
          $("#chart_name").text("Graphical Representation of  Performance of "+ which_equip);
          $("#start_b").attr('onclick', 'start('+'"'+which_equip+'"'+')');
          $("#stop_b").attr('onclick', 'stop('+'"'+which_equip+'"'+')');
          refreshdata(which_equip);
       }
    });
};

function getlivechart(now) {
    test_array = []
    $("#chart_name").show();
    $("#oven").show();
    $.ajax({
       "url": "/api/trends/PowderOven/",
       "method": "POST",
       "data":{
          'inside_temp':"TDR-101-Return Temperature",
          'inside_hum':"TDR-102-Inside Temperature",
          'ambient_temp':"TDR-103-Supply Temperature"
       },
       success:function(data) {
          for(i=0;i<data['inside_temp'].length;i++){
             ret_temp.push(data['inside_temp'][i]['data']);
             date1.push(data['inside_temp'][i]['created']);
             test_array.push({ 'x' : data['inside_temp'][i]['created'], 'y':data['inside_temp'][i]['data']},);
          }
          for(i=0;i<data['ambient_temp'].length;i++){
             supp.push(data['ambient_temp'][i]['data']);
          }
          for(i=0;i<data['inside_hum'].length;i++){
             ins_temp.push(data['inside_hum'][i]['data']);
          }
          infoovenchart();
       }
    });
}        


function infoovenchart() {
    var myChart1 = echarts.init(document.getElementById('oven'),'infographic');
    myChart1.showLoading();
    var option = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                animation: true,
                label: {
                    backgroundColor: '#505765'}
                }
            },
        grid: {
            bottom: 80
        },
        legend: {
            x:'left',
            data:['Supply Temp.', 'Return Temp.', 'Inside Temp.',],
            textStyle:{
                color: '#fff',
                fonstSize: 14
            },
        },
        dataZoom: [{
                show: true,
                realtime: true,
                start: 75,
                end: 100
            },
            {
                type: 'inside',
                realtime: true,
                start: 85,
                end: 100
            }],
        xAxis: {
            type : 'category',
            boundaryGap : false,
            axisLabel: {
                color:'#fff',
                fontSize: 10,
                margin:5,
                padding: 5,
                
            },
            axisLine: {onZero: true},
            // boundaryGap: ['95%'],
            data: date1
        },
        yAxis: [{
            type: 'value',
            axisLabel: {
                color:'#fff',
                fontSize: 10,
                margin:5,
                padding: [0, 0, 0, 20],
                formatter: '{value} °C'
            }
        }],
        series: [{
                name: 'Supply Temp.',
                type: 'line',
                data: supp
            },
            {
                name: 'Return Temp.',
                type: 'line',
                data: ret_temp
            },
            {
                name: 'Inside Temp.',
                type: 'line',
                data: ins_temp
            }]
    };
    
    setInterval(function (){
        axisData = (new Date()).toLocaleTimeString().replace(/^\D*/,'');
        var data0 = option.series[0].data;
        var data1 = option.series[1].data;
        var data2 = option.series[2].data;
        $.ajax({
           "url": "/api/trends/PowderOven/",
           "method": "POST",
           "data":{
                'inside_temp':"TDR-101-Return Temperature",
                'inside_hum':"TDR-102-Inside Temperature",
                'ambient_temp':"TDR-103-Supply Temperature"
                },
            success:function(data) {
                for(i=0;i<data['inside_temp'].length;i++){
                    data0.shift();
                    data0.push(data['inside_temp'][i]['data']);
                    option.xAxis.data.shift();
                    option.xAxis.data.push(data['inside_temp'][i]['created']);
                }
                for(i=0;i<data['ambient_temp'].length;i++){
                    data1.shift();
                    data1.push(data['ambient_temp'][i]['data']);
                }
                for(i=0;i<data['inside_hum'].length;i++){
                    data2.shift();
                    data2.push(data['inside_hum'][i]['data']);
                }
            }
        });
        
        if (option && typeof option === "object") {
            myChart1.hideLoading();
            myChart1.setOption(option, true), $(function() {
                function resize() {
                    setTimeout(function() {
                        myChart1.resize()
                    }, 100)
                }
                $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
            });
        };
    }, 5000);
}

function start(equip){
    $("#"+equip).css({ fill: "#00FF00" });
    $("#equip_img").attr('src', "/static/mimics/pump/pumpt1green.png");
}

function stop(equip){
    $("#"+equip).css({ fill: "#ff0000" });
    $("#equip_img").attr('src', "/static/mimics/pump/bgfsdhg.png");
    // return false;
}