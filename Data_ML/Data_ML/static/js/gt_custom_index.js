$('.dateranges').daterangepicker({
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
});


$('#example23').DataTable({
  dom: 'Bfrtip',
  buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
  ]
});


var pieChart = echarts.init(document.getElementById('pie-chart'),  'infographic');

// specify chart configuration item and data
option = {

  tooltip: {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
  },
  legend: {
      x: 'center',
      y: 'bottom',
      data: ['VTC', 'ETC', 'MAB', 'WAB'],
      textStyle:{color:'#fff'}
  },
  toolbox: {
      show: true,
      feature: {

        //   dataView: { show: true, readOnly: false },
          magicType: {
              show: true,
              type: ['pie', 'funnel',]
          },
          restore: { show: true },
          saveAsImage: { show: true }
      },
      color:['#fff', '#fff', '#fff', '#fff', '#fff']
  },
//   color: ["#55ce63", "#ffbc34", "#009efb"],
  calculable: true,
  series: [
      {

                name: 'Radius mode',
                type: 'pie',
                radius: [20, 70],
                center: ['50%', 110],
                roseType: 'radius',
                width: '100%', // for funnel
                max: 40, // for funnel
                itemStyle: {
                    normal: {
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        },
                        labelLine: {
                            show: true
                        }
                    }
                },
                data: [
                    { value: 10, name: 'VTC' },
                    { value: 5, name: 'ETC' },
                    { value: 15, name: 'MAB' },
                    { value: 12, name: 'WAB' },

                ]
            },

  ]
};



// use configuration item and data specified to show chart
pieChart.setOption(option, true), $(function() {
  function resize() {
      setTimeout(function() {
          pieChart.resize()
      }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
var pieChart = echarts.init(document.getElementById('pie-chart1'), 'infographic');

// specify chart configuration item and data
option = {

  tooltip: {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
  },
  legend: {
      x: 'center',
      y: 'bottom',
      data: ['VTC', 'ETC', 'MAB', 'WAB'],
      textStyle:{color:'#fff'}
  },
  toolbox: {
      show: true,
      feature: {

        //   dataView: { show: true, readOnly: false },
          restore: { show: true },
          saveAsImage: { show: true },
      },
      color:['#fff', '#fff', '#fff', '#fff', '#fff']
  },
//   color: ["#55ce63", "#ffbc34", "#009efb"],
  calculable: true,
  series: [
      {

                // name: 'Radius mode',
                type: 'pie',
                radius: [20, 70],
                center: ['50%', 110],
                roseType: 'radius',
                width: '100%', // for funnel
                max: 40, // for funnel
                itemStyle: {
                    normal: {
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        },
                        labelLine: {
                            show: true
                        }
                    }
                },
                data: [
                    { value: 10, name: 'VTC' },
                    { value: 5, name: 'ETC' },
                    { value: 15, name: 'MAB' },
                    { value: 12, name: 'WAB' },

                ]
            },

  ]
};

// use configuration item and data specified to show chart
pieChart.setOption(option, true), $(function() {
  function resize() {
      setTimeout(function() {
          pieChart.resize()
      }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
var pieChart = echarts.init(document.getElementById('pie-chart2'), 'infographic');

// specify chart configuration item and data
option = {

  tooltip: {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
  },
  legend: {
      x: 'center',
      y: 'bottom',
      data: ['VTC', 'ETC', 'MAB', 'WAB'],
      textStyle:{color:'#fff'}
  },
  toolbox: {
      show: true,
      feature: {

        //   dataView: { show: true, title: "Data View", readOnly: false },
          magicType: {
              show: true,
              type: ['pie', 'funnel']
          },
          restore: { show: true },
          saveAsImage: { show: true }
      },
      color:['#fff', '#fff', '#fff', '#fff', '#fff']
  },
//   color: ["#55ce63", "#ffbc34", "#009efb"],
  calculable: true,
  series: [
      {

                // name: 'Radius mode',
                type: 'pie',
                radius: [20, 70],
                center: ['50%', 110],
                roseType: 'radius',
                width: '100%', // for funnel
                max: 40, // for funnel
                itemStyle: {
                    normal: {
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        },
                        labelLine: {
                            show: true
                        }
                    }
                },
                data: [
                    { value: 10, name: 'VTC' },
                    { value: 5, name: 'ETC' },
                    { value: 15, name: 'MAB' },
                    { value: 11,  name: 'WAB' },

                ]
            },

  ]
};



// use configuration item and data specified to show chart
pieChart.setOption(option, true), $(function() {
  function resize() {
      setTimeout(function() {
          pieChart.resize()
      }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});
var pieChart = echarts.init(document.getElementById('pie-chart3'));

// specify chart configuration item and data
option = {

  tooltip: {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
  },
  legend: {
      x: 'center',
      y: 'bottom',
      data: ['VTC', 'ETC', 'MAB'],
      textStyle:{color:'#fff'}
  },
  toolbox: {
      show: true,
      feature: {

        //   dataView: { show: true, readOnly: false },
          magicType: {
              show: true,
              type: ['pie', 'funnel',]
          },
          restore: { show: true },
          saveAsImage: { show: true }
      },
      color:['#fff', '#fff', '#fff', '#fff', '#fff']
  },
  color: ["#55ce63", "#ffbc34", "#009efb"],
  calculable: true,
  series: [
      {

                // name: 'Radius mode',
                type: 'pie',
                radius: [20, 70],
                center: ['50%', 110],
                roseType: 'radius',
                width: '100%', // for funnel
                max: 40, // for funnel
                itemStyle: {
                    normal: {
                        label: {
                            show: false
                        },
                        labelLine: {
                            show: false
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        },
                        labelLine: {
                            show: true
                        }
                    }
                },
                data: [
                    { value: 10, name: 'VTC' },
                    { value: 5, name: 'ETC' },
                    { value: 15, name: 'MAB' },
                    { value: 12, name: 'WAB' },

                ]
            },

  ]
};



// use configuration item and data specified to show chart
pieChart.setOption(option, true), $(function() {
  function resize() {
      setTimeout(function() {
          pieChart.resize()
      }, 100)
  }
  $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
});

// based on prepared DOM, initialize echarts instance
function Versuschart(){


var dom = document.getElementById("main");
var mytempChart = echarts.init(dom, 'infographic');
// va
var app = {};
option = null;
option = {

  tooltip: {
      trigger: 'axis'
  },
  legend: {
      data: ['Temprature', 'Humidity']
  },
  toolbox: {
      show: true,
      feature: {
          magicType: { show: true, type: ['line', 'bar'] },
          restore: { show: true },
          saveAsImage: { show: true }
      }
  },
  color: ["#55ce63", "#009efb"],
  calculable: true,
  xAxis: [{
      type: 'category',
      boundaryGap: false,
      data: date
  }],
  yAxis: [{
      type: 'value',
      axisLabel: {
          formatter: '{value} °C'
      }
  }],

  series: [{
          name: 'Temprature',
          type: 'line',
          color: ['#000'],
          data: inside_temp_array,
          markPoint: {
              data: [
                  { type: 'max', name: 'Max' },
                  { type: 'min', name: 'Min' }
              ]
          },
          itemStyle: {
              normal: {
                  lineStyle: {
                      shadowColor: 'rgba(0,0,0,0.3)',
                      shadowBlur: 10,
                      shadowOffsetX: 8,
                      shadowOffsetY: 8
                  }
              }
          },
          markLine: {
              data: [
                  { type: 'average', name: 'Average' }
              ]
          }
      },
      {
          name: 'Humidity',
          type: 'line',
          data: inside_hum_array,
          markPoint: {
              data: [
                  { name: 'Week minimum', value: -2, xAxis: 1, yAxis: -1.5 }
              ]
          },
          itemStyle: {
              normal: {
                  lineStyle: {
                      shadowColor: 'rgba(0,0,0,0.3)',
                      shadowBlur: 10,
                      shadowOffsetX: 8,
                      shadowOffsetY: 8
                  }
              }
          },
          markLine: {
              data: [
                  { type: 'average', name: 'Average' }
              ]
          }
      }
  ]
};

if (option && typeof option === "object") {
  mytempChart.setOption(option, true), $(function() {
      function resize() {
          setTimeout(function() {
              mytempChart.resize()
          }, 100)
      }
      $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
  });
}
}
